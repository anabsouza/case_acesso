package br.com.itau.CaseAcesso.Controller;


import br.com.itau.CaseAcesso.Models.Cliente;
import br.com.itau.CaseAcesso.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController
{
    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente criar(@RequestBody @Valid Cliente cliente)
    {
        return  clienteService.criar(cliente);
    }

    @GetMapping("/{id}")
    public Cliente buscarClientePorId(@PathVariable(name = "id") int id)
    {
           return clienteService.buscarClientePorId(id);
          }
}



