package br.com.itau.CaseAcesso.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.CaseAcesso.Models.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente,Integer > {

}




