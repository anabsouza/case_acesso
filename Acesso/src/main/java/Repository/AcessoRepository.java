package Repository;


import Modell.Acesso;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

@Repository
public interface AcessoRepository extends JpaRepository<Acesso, Integer> {

    Optional<Acesso> findFirstByAndar(String andar);
}

