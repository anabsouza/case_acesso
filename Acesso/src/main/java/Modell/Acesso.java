package Modell;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Acesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Andar é necessário informar")
    private String andar;

    @NotBlank(message = "Sala é necessário informar")
    private String sala;

    public Acesso() {
    }

//** ID

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//** Andar
    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {

        this.andar = andar;
    }

 //** Sala

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

}

