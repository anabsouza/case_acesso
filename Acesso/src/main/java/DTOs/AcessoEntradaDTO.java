package DTOs;

import Modell.Acesso;

public class AcessoEntradaDTO {

    private int clienteId;

    private String andar;

    private String sala;

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return andar;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }


    public Acesso converterParaPorta() {
        Acesso acesso = new Acesso();

        acesso.setAndar(this.andar);

        return acesso;

    }
}
