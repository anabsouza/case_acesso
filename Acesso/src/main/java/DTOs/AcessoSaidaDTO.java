package DTOs;

import Modell.Acesso;
import br.com.itau.CaseAcesso.Models.Cliente;

public class AcessoSaidaDTO {

    private int id;

    private int clienteId;

    private String andar;

    private String sala;

    private Cliente cliente;

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AcessoSaidaDTO(Acesso acesso, Cliente cliente) {

        this.setClienteId(cliente.getId());
        this.setAndar(acesso.getAndar());
        this.setSala(acesso.getSala());
        this.setId(acesso.getId());



    }

}
