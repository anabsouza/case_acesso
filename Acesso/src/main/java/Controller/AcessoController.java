package Controller;


import DTOs.AcessoEntradaDTO;
import DTOs.AcessoSaidaDTO;
import Modell.Acesso;
import Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
//@RequestMapping("/cartoes")
public class AcessoController {


    @Autowired
    private AcessoService portaServer;


    @PostMapping("/solicitarnovoandar")
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoSaidaDTO solicitaNovoAndar(@RequestBody AcessoEntradaDTO acessoEntradaDTO) {

        return portaServer.solicitaNovoandar(acessoEntradaDTO);


    }


    //Buscar Cartão.
    @GetMapping(value = "/buscarpornumero/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public Acesso buscarPortaPorId(@RequestBody @PathVariable(name = "porta") String numero) {
        return portaServer.buscarPortaPorId(numero);

    }

    //Buscar porta pelo ID.
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Acesso buscarPortaPorIdS(@PathVariable(name = "id") int id) {

        return portaServer.buscarPortaPorIdS(id);
    }


}
