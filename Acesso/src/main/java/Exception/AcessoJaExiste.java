package Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FOUND, reason = "Acesso já cadastrada!")
public class AcessoJaExiste extends  RuntimeException {
