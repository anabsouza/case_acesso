package Service;


import DTOs.AcessoEntradaDTO;
import DTOs.AcessoSaidaDTO;
import Modell.Acesso;
import Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {


    @Autowired
    AcessoRepository acessoRepository;


    @Autowired
    ClientePorta clientePorta;


    public AcessoSaidaDTO solicitaNovaPorta(AcessoEntradaDTO acessoEntradaDTO) {

        Optional<Acesso> cartaoOptional = acessoRepository.findFirstByAndar(acessoEntradaDTO.getAndar());

        if (!portaOptional.isPresent()) {

            Acesso acesso = new Acesso();
            Cliente cliente = clientePorta.buscarClientePeloId(acessoEntradaDTO.getClienteId());



            acesso.setAndar(acessoEntradaDTO.getAndar());
            acesso.setSala(acessoEntradaDTO.getSala());
            acesso.setClienteId(acessoEntradaDTO.getClienteId());

            acesso = acessoRepository.save(acesso);

            return new AcessoSaidaDTO(acesso, cliente);
        } else {
            throw new AcessoJaExiste();
        }

    }


    //Busca Acesso pelo andar.
    public Acesso buscarCartaoPeloAndar(String numero) {


        Optional<Acesso> cartaoOptional = acessoRepository.findFirstByAndar(andar);

        if (portaOptional.isPresent()) {
            Acesso acesso = portaOptional.get();
            return acesso;
        } else {
            throw new PortaNaoEncontrado();
        }

    }

    public Cartao buscarCartaoPeloId(int id) {
        Optional<Cartao> clienteOptional = cartaoRepository.findById(id);

        if (clienteOptional.isPresent()) {
            Cartao cartao = clienteOptional.get();
            return cartao;
        } else {
            throw new CartaoNaoEncontrado();
        }


    }
}
