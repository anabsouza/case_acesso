package br.com.itau.CaseAcesso.Controller;


import br.com.itau.CaseAcesso.Models.Porta;
import br.com.itau.CaseAcesso.Service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController
{
    @Autowired
    PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta criarPorta(@RequestBody @Valid Porta porta)
    {
        return portaService.criarPorta(porta);
    }

    @GetMapping("/{id}")
    public Porta buscarClientePorPorta(@PathVariable(name = "id") int id)
    {
           return portaService.consultarPortaPorId(id);
          }
}



