package br.com.itau.CaseAcesso.Repository;

import br.com.itau.CaseAcesso.Models.Porta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortaRepository extends JpaRepository<Porta,Integer > {

}




